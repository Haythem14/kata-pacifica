package com.pacifica.calculatetax.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.pacifica.calculatetax.enums.ProductType;
import com.pacifica.calculatetax.models.Product;
import com.pacifica.calculatetax.services.BookTaxCalculator;
import com.pacifica.calculatetax.services.FirstNecessityTaxCalculator;
import com.pacifica.calculatetax.services.ImportedTaxCalculator;
import com.pacifica.calculatetax.services.OtherTaxCalculator;

public class TaxCalculatorStrategyTest {

	@InjectMocks
	OtherTaxCalculator otherTaxCalculator;

	@InjectMocks
	BookTaxCalculator bookTaxCalculator;

	@InjectMocks
	FirstNecessityTaxCalculator firstNecessityTaxCalculator;

	@InjectMocks
	ImportedTaxCalculator importedTaxCalculator;

	private Product product;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		product = new Product("PC", false, BigDecimal.valueOf(14.99).setScale(2), ProductType.OTHER);
	}

	@Test
	public void otherTaxcalculateTest() {

		Double expectedTax = 2.998;
		BigDecimal actualTax = otherTaxCalculator.calculate(product);

		assertEquals(BigDecimal.valueOf(expectedTax), actualTax);
	}

	@Test
	public void firstNecessityTaxcalculateTest() {

		product.setName("Doliprane");
		product.setType(ProductType.FIRST_NECESSITY);

		BigDecimal expectedTax = new BigDecimal(0);
		BigDecimal actualTax = firstNecessityTaxCalculator.calculate(product);

		assertEquals(expectedTax, actualTax);
	}

	@Test
	public void bookTaxcalculateTest() {

		product.setName("Game of thrones");
		product.setType(ProductType.BOOK);

		Double expectedTax = 1.499;
		BigDecimal actualTax = bookTaxCalculator.calculate(product);

		assertEquals(BigDecimal.valueOf(expectedTax), actualTax);
	}

	@Test
	public void importedTaxcalculateTest() {

		product.setName("parfum");
		product.setType(ProductType.OTHER);
		product.setImported(true);

		Double expectedTax = 0.7495;
		BigDecimal actualTax = importedTaxCalculator.calculate(product);

		assertEquals(BigDecimal.valueOf(expectedTax), actualTax);
	}

}
