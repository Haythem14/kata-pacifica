package com.pacifica.calculatetax.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.pacifica.calculatetax.enums.ProductType;
import com.pacifica.calculatetax.models.Bill;
import com.pacifica.calculatetax.models.Order;
import com.pacifica.calculatetax.models.Product;
import com.pacifica.calculatetax.services.BookTaxCalculator;
import com.pacifica.calculatetax.services.CalculateTax;
import com.pacifica.calculatetax.services.FirstNecessityTaxCalculator;
import com.pacifica.calculatetax.services.ImportedTaxCalculator;
import com.pacifica.calculatetax.services.OtherTaxCalculator;

public class CalculateTaxServiceTest {

	@Mock
	OtherTaxCalculator otherTaxCalculator;

	@Mock
	BookTaxCalculator bookTaxCalculator;

	@Mock
	FirstNecessityTaxCalculator firstNecessityTaxCalculator;

	@Mock
	ImportedTaxCalculator importedTaxCalculator;

	@InjectMocks
	CalculateTax calculateTax;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void generateBillTest() {
		List<Order> order = new ArrayList<>();
		order.add(new Order(createBookProduct(), 2L));
		order.add(new Order(createFirstNecessityProduct(), 3L));
		order.add(new Order(createOtherProduct(), 1L));
		order.add(new Order(createOtherImportedProduct(), 1L));

		when(firstNecessityTaxCalculator.calculate(any(Product.class))).thenReturn(BigDecimal.valueOf(0));
		when(bookTaxCalculator.calculate(any(Product.class))).thenReturn(BigDecimal.valueOf(1.249));
		when(otherTaxCalculator.calculate(any(Product.class))).thenReturn(BigDecimal.valueOf(2.24));
		when(importedTaxCalculator.calculate(any(Product.class))).thenReturn(BigDecimal.valueOf(0.9495));

		Bill bill = calculateTax.generateBill(order);
		assertEquals(order.size(), bill.getOrdersList().size());

		Double expectedTax = 9.50;
		assertEquals(BigDecimal.valueOf(expectedTax).setScale(2), bill.getTotalTax());

		Double expectedTotalTTC = 94.10;
		assertEquals(BigDecimal.valueOf(expectedTotalTTC).setScale(2), bill.getTotalTTC());

	}

	private Product createBookProduct() {
		Product book = new Product("livre", false, BigDecimal.valueOf(12.49), ProductType.BOOK);
		return book;
	}

	private Product createFirstNecessityProduct() {
		return new Product("chocolat", false, BigDecimal.valueOf(9.8), ProductType.FIRST_NECESSITY);
	}

	private Product createOtherProduct() {
		return new Product("parfum", false, BigDecimal.valueOf(11.2), ProductType.OTHER);
	}

	private Product createOtherImportedProduct() {
		return new Product("parfum importé", true, BigDecimal.valueOf(18.99), ProductType.OTHER);
	}

}
