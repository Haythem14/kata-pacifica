package com.pacifica.calculatetax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.pacifica.calculatetax.enums.ProductType;
import com.pacifica.calculatetax.factory.OrderFactory;
import com.pacifica.calculatetax.models.Bill;
import com.pacifica.calculatetax.models.Order;
import com.pacifica.calculatetax.services.CalculateTax;
import com.pacifica.calculatetax.services.CalculateTaxService;

public class App {
	public static void main(String[] args) {

		List<Order> ordersList1 = generateInput1();
		List<Order> ordersList2 = generateInput2();
		List<Order> ordersList3 = generateInput3();

		CalculateTaxService calculateTaxService = new CalculateTax();

		Bill bill1 = calculateTaxService.generateBill(ordersList1);
		Bill bill2 = calculateTaxService.generateBill(ordersList2);
		Bill bill3 = calculateTaxService.generateBill(ordersList3);

		System.out.println(bill1.toString());
		System.out.println("----------------------------------------------------------");
		System.out.println(bill2.toString());
		System.out.println("----------------------------------------------------------");
		System.out.println(bill3.toString());

	}

	private static List<Order> generateInput1() {

		OrderFactory orderFactory = new OrderFactory();
		List<Order> ordersList = new ArrayList<>();

		ordersList.add(orderFactory.createOrder("livre", false, BigDecimal.valueOf(12.49), ProductType.BOOK, 2L));
		ordersList.add(orderFactory.createOrder("CD musical", false, BigDecimal.valueOf(14.99), ProductType.OTHER, 1L));
		ordersList.add(orderFactory.createOrder("barres de chocolat", false, BigDecimal.valueOf(0.85),
				ProductType.FIRST_NECESSITY, 3L));

		return ordersList;
	}

	private static List<Order> generateInput2() {

		OrderFactory orderFactory = new OrderFactory();

		List<Order> ordersList = new ArrayList<>();
		ordersList.add(orderFactory.createOrder("boîtes de chocolats", true, BigDecimal.valueOf(10),
				ProductType.FIRST_NECESSITY, 2L));
		ordersList.add(orderFactory.createOrder("flacons de parfum importé", true, BigDecimal.valueOf(47.50),
				ProductType.OTHER, 3L));

		return ordersList;
	}

	private static List<Order> generateInput3() {

		OrderFactory orderFactory = new OrderFactory();

		List<Order> ordersList = new ArrayList<>();
		ordersList.add(orderFactory.createOrder("flacons de parfum importé", true, BigDecimal.valueOf(27.99),
				ProductType.OTHER, 2L));
		ordersList.add(
				orderFactory.createOrder("flacon de parfum", false, BigDecimal.valueOf(18.99), ProductType.OTHER, 1L));
		ordersList.add(orderFactory.createOrder("boîtes de pilules contre la migraine", false, BigDecimal.valueOf(9.75),
				ProductType.FIRST_NECESSITY, 3L));
		ordersList.add(orderFactory.createOrder("boîtes de chocolats importés", true, BigDecimal.valueOf(11.25),
				ProductType.FIRST_NECESSITY, 2L));

		return ordersList;
	}
}
