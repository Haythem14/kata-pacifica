package com.pacifica.calculatetax.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {

	public static BigDecimal roundToNearest5Cents(final Double value) {
		return BigDecimal.valueOf((Math.ceil(value * 20) / 20)).setScale(2, RoundingMode.HALF_UP);
	}

}
