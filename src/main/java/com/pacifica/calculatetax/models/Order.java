package com.pacifica.calculatetax.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.pacifica.calculatetax.constants.TaxCalculatorConstants;

public class Order {

	private Product product;

	private Long quantity;

	private BigDecimal priceTTC;

	private BigDecimal tax;

	public Order() {
	}

	public Order(Product product, Long quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public Order(Product product, Long quantity, BigDecimal priceTTC) {
		this.product = product;
		this.quantity = quantity;
		this.priceTTC = priceTTC;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPriceTTC() {
		return priceTTC;
	}

	public void setPriceTTC(BigDecimal priceTTC) {
		this.priceTTC = priceTTC;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	@Override
	public String toString() {

		DecimalFormat df = new DecimalFormat(TaxCalculatorConstants.DECIMAL_FORMAT);
		df.setRoundingMode(RoundingMode.CEILING);
		return quantity + " " + product.getName() + " à " + product.getPrice() + "€" + " : " + df.format(priceTTC) + "€"
				+ " TTC";
	}

}
