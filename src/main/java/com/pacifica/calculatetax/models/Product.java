package com.pacifica.calculatetax.models;

import java.math.BigDecimal;

import com.pacifica.calculatetax.enums.ProductType;

public class Product {

	private String name;

	private Boolean imported;

	private BigDecimal price;

	private ProductType type;

	public Product() {
	}

	public Product(String name, Boolean imported, BigDecimal price, ProductType type) {
		this.name = name;
		this.imported = imported;
		this.price = price;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isImported() {
		return imported;
	}

	public void setImported(Boolean imported) {
		this.imported = imported;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

}
