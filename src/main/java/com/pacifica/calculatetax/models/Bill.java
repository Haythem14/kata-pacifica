package com.pacifica.calculatetax.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

import com.pacifica.calculatetax.constants.TaxCalculatorConstants;

public class Bill {

	private List<Order> ordersList;

	private BigDecimal totalTax = new BigDecimal(0);

	private BigDecimal totalTTC = new BigDecimal(0);

	public Bill() {
		super();
	}

	public Bill(List<Order> ordersList, BigDecimal totalTax, BigDecimal totalTTC) {
		this.ordersList = ordersList;
		this.totalTax = totalTax;
		this.totalTTC = totalTTC;
	}

	public List<Order> getOrdersList() {
		return ordersList;
	}

	public void setOrdersList(List<Order> ordersList) {
		this.ordersList = ordersList;
	}

	public BigDecimal getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	public BigDecimal getTotalTTC() {
		return totalTTC;
	}

	public void setTotalTTC(BigDecimal totalTTC) {
		this.totalTTC = totalTTC;
	}

	@Override
	public String toString() {

		DecimalFormat df = new DecimalFormat(TaxCalculatorConstants.DECIMAL_FORMAT);
		df.setRoundingMode(RoundingMode.CEILING);

		StringBuilder str = new StringBuilder("");

		str.append(ordersList.stream().map(Object::toString).collect(Collectors.joining("\n"))).append("\n");
		str.append('\n');
		str.append("Montant des taxes : ");
		str.append(df.format(totalTax)).append('€');
		str.append('\n');
		str.append("Total : ");
		str.append(df.format(totalTTC)).append('€');
		return str.toString();
	}

}
