package com.pacifica.calculatetax.factory;

import java.math.BigDecimal;

import com.pacifica.calculatetax.enums.ProductType;
import com.pacifica.calculatetax.models.Order;
import com.pacifica.calculatetax.models.Product;

public class OrderFactory {

	public Order createOrder(String name, Boolean imported, BigDecimal price, ProductType type, Long quantity) {
		Product product = new Product(name, imported, price, type);
		return new Order(product, quantity);
	}
}
