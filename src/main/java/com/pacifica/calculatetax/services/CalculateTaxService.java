package com.pacifica.calculatetax.services;

import java.util.List;

import com.pacifica.calculatetax.models.Bill;
import com.pacifica.calculatetax.models.Order;

public interface CalculateTaxService {
	Bill generateBill(List<Order> orderList);

}
