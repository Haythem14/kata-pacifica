package com.pacifica.calculatetax.services;

import java.math.BigDecimal;
import java.util.List;

import com.pacifica.calculatetax.models.Bill;
import com.pacifica.calculatetax.models.Order;
import com.pacifica.calculatetax.utils.Utils;

public class CalculateTax implements CalculateTaxService {

	private void applyTax(Order order) {

		TaxCalculatorStrategy taxCalculatorStrategy;

		switch (order.getProduct().getType()) {
		case FIRST_NECESSITY:
			taxCalculatorStrategy = new FirstNecessityTaxCalculator();
			break;
		case BOOK:
			taxCalculatorStrategy = new BookTaxCalculator();
			break;
		case OTHER:
			taxCalculatorStrategy = new OtherTaxCalculator();
			break;
		default:
			taxCalculatorStrategy = new FirstNecessityTaxCalculator();
			break;
		}

		BigDecimal tax = taxCalculatorStrategy.calculate(order.getProduct());

		if (Boolean.TRUE.equals(order.getProduct().isImported())) {

			taxCalculatorStrategy = new ImportedTaxCalculator();
			tax = tax.add(taxCalculatorStrategy.calculate(order.getProduct()));
		}

		order.setTax(calculateTaxValueForOrder(tax, order));
		order.setPriceTTC(calculatePriceTTC(order));
	}

	@Override
	public Bill generateBill(List<Order> ordersList) {
		
		Bill bill = new Bill();
		bill.setOrdersList(ordersList);
		
		ordersList.stream().forEach(order -> {
			applyTax(order);
			bill.setTotalTax(bill.getTotalTax().add(order.getTax()));
			bill.setTotalTTC(bill.getTotalTTC().add(order.getPriceTTC()));
		});
		return bill;
	}

	private BigDecimal calculatePriceTTC(Order order) {
		return Utils.roundToNearest5Cents(order.getTax()
						.add((order.getProduct().getPrice()
						.multiply(BigDecimal.valueOf(order.getQuantity())))).doubleValue());
	}

	private BigDecimal calculateTaxValueForOrder(BigDecimal tax, Order order) {
		return Utils.roundToNearest5Cents(tax.multiply(BigDecimal.valueOf(order.getQuantity())).doubleValue());
	}

}
