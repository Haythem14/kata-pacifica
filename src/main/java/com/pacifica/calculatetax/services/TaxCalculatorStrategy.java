package com.pacifica.calculatetax.services;

import java.math.BigDecimal;

import com.pacifica.calculatetax.models.Product;

public interface TaxCalculatorStrategy {

	BigDecimal calculate(Product product);

}
