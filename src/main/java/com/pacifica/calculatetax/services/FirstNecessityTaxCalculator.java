package com.pacifica.calculatetax.services;

import java.math.BigDecimal;

import com.pacifica.calculatetax.models.Product;

public class FirstNecessityTaxCalculator implements TaxCalculatorStrategy {

	public BigDecimal calculate(Product product) {
		return new BigDecimal(0);
	}
}
