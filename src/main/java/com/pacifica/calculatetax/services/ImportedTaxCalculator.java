package com.pacifica.calculatetax.services;

import java.math.BigDecimal;

import com.pacifica.calculatetax.constants.TaxCalculatorConstants;
import com.pacifica.calculatetax.models.Product;

public class ImportedTaxCalculator implements TaxCalculatorStrategy {

	public BigDecimal calculate(Product product) {
		return (product.getPrice().multiply(BigDecimal.valueOf(TaxCalculatorConstants.FIVE)))
				.divide(BigDecimal.valueOf(100));
	}
}
