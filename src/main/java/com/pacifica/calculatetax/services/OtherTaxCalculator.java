package com.pacifica.calculatetax.services;

import java.math.BigDecimal;

import com.pacifica.calculatetax.constants.TaxCalculatorConstants;
import com.pacifica.calculatetax.models.Product;

public class OtherTaxCalculator implements TaxCalculatorStrategy {

	public BigDecimal calculate(Product product) {
		return (product.getPrice().multiply(BigDecimal.valueOf(TaxCalculatorConstants.TWENTY)))
				.divide(BigDecimal.valueOf(100));
	}
}
