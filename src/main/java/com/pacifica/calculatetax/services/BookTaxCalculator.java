package com.pacifica.calculatetax.services;

import java.math.BigDecimal;

import com.pacifica.calculatetax.constants.TaxCalculatorConstants;
import com.pacifica.calculatetax.models.Product;

public class BookTaxCalculator implements TaxCalculatorStrategy {

	public BigDecimal calculate(Product product) {
		return (product.getPrice().multiply(BigDecimal.valueOf(TaxCalculatorConstants.TEN)))
				.divide(BigDecimal.valueOf(100));
	}

}
