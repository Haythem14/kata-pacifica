package com.pacifica.calculatetax.constants;

public final class TaxCalculatorConstants {

	private TaxCalculatorConstants() {
	}

	public static final String DECIMAL_FORMAT = "#.###";

	public static final int FIVE = 5;

	public static final int TEN = 10;

	public static final int TWENTY = 20;

}
