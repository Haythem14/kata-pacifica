package com.pacifica.calculatetax.enums;

public enum ProductType {
	FIRST_NECESSITY,
	BOOK,
	OTHER
}
